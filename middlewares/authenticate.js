const config = require('./../configs');
const jwt = require('jsonwebtoken');
const UserModel = require('./../models/user.model');
module.exports = function (req, res, next) {

    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']

    if (req.headers['x-acess-token'])
        token = req.headers['x-access-token']

    if (req.query.token)
        token = req.query.token;

    if (token) {
        // token verification
        jwt.verify(token, config.JWT_SECRET, function (err, done) {
            if (err) {
                return next(err);
            }
            // token verification complete
            UserModel.findById(done._id, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: "Authentication Failed, User Not Found",
                        status: 401
                    })
                }

                req.user = user;
                next();
            })
        })

    } else {
        next({
            msg: "Authentication Failed, Token Not Provided",
            status: 401
        })
    }

}


// TODO create a authroize middleware 
// check weather the user is admin or not
