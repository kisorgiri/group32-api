module.exports = function (req, res, next) {
    if (req.query.isAdmin) {
        next();
    } else {
        next({
            msg: "You dont have access",
            status: 403
        })
    }
}