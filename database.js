// database is a container where data are kept

// database management system is a way to manipulate data in database

// DBMS
// 1. Relational DBMS
// 2. Distributed DBMS

// 1. Relational DBMS
// a. table based design
// eg of LMS ==> Books ,Users, Reviews
// b. tuple/row ==> each record inside a table
// c. Schema Based Solution 
// d. Non Scalable
// e. Relation between table exists
// f.SQL database (Structure Query Languages)
// g.mysql,sql-lite,postgres........

// 2.Distributed DBMS
// a.Collection based design
// eg of LMS ==> Books, Users, Reviews
// b.each record inside a collection is document
// document based database
// document is a valid JSON
// c Schema less design
// d. highly scalable
// e. relation doesnot exists between collections
// f.NoSQL (Not Only SQL)
// g.Mongodb, redis, dynamoDb, cosmos db


// to setup mongodb
// install mongodb current version
// your installed file will be available
//c drive/ programmefiles/==> mongodb ==>version / bin / executable files
// copy the entire path upto bin directory[ in the url bar]

// right click to my computer
// advance system settings
// environment variables
// system variables
// path ==>
// add/edit
// paste the copied url

// mongo 
// mongod ==> driver initilization


// to access mongo shell
// mongo

// shell command

// show dbs  ==> list all the available database of the system
// use <db_name> ==> create or select the database
// if(db_name exists) select the exisiting db
// else create and select the database


// db ==> check selected database

//show collections ==> list all the available collection of selected database

// CRUD (CREATE ,READ, UPDATE , DELETE)

// Create
// db.<collection_name [new or existing]>.insert({valid JSON})
// db.<collection_name>.insertMany()

// Read
// db.<collection_name>.find({query_builder});
// db.<collection_name>.find({query_builder}).pretty() // format output
// db.<collection_name>.find({query_builder}).count() // returns count
// skip
    // query.skip(skipCount)
// limit 
// query.limit('limitCount')
// sort
// .sort({_id:-1}) // decending

// projection
// db.<collection_name>.find({condition},{projection_object}) //either inclusion or exclusion

// Update
// db.<collection_name>.update({},{},{});
// first object is query builder
// 2nd object must have $set as key as value of another object[data to be updated]
// 3rd object is optional 


// Remove
// db.<collection_name>.remove({query_builder});
// NOTE:- DO NOT KEPT EMPTY QUERY BUILDER

// drop collections
// db.<collection_name>.drop();

// drop database 
// db.dropDatabase();


// MONGOOSE===> ODM (Object Document Modelling)
// 
// advantages or usage of Mongoose 

// 1. schema based solution
// 2. Indexing is lot more easier 
// required, unique
// 3. own methods of ODM for (CRUD)
// 4. data type
// 5. middleware    


// ****************DATABASE BACKUP & RESTORE******************
// format ==> json/csv , bson
// command ==> mongodump, mongorestore  mongoimport mongoexport

// bson
// backup
// command ==> mongodump
// mongodump ==>  it will create a default dump folder and backup all the available database 
// mongodump --db <db_name> it will back a selected db into default dump folder
// mongodump --db <db_name> --out <path_to_desitnation_folder>

// restore
// command ==> mongorestore
// mongorestore ==> it will check for dump folder and restore all the database available inside dump folder
// mongorestore --drop <drop existing db> and restore from backup from dump folder
// mongorestore --drop <path_to_source_folder>

//json and csv
// command mongoimport and mongoexport
// json
// backup
// command 
// mongoexport
// mongoexport --db <db_name> --collection<collection_name> --out <path_to_desitnation_file_with.json extension>
// mongoexport -d <db_name> -c<collection_name> -o <path_to_desitnation_file_with.json extension>

// restore
// command 
// mongoimport
// mongoimport --db <new_db or existing _db> --collection<new or existing collection> path_to_json file

// csv
// backup command mongoexport
// mongoexport --db <db_name> --collection <collection_name> --type=csv --fields 'comma seperated value to be exported' --out <path_to_destination_with.csv_extenstion>
// mongoexport --db <db_name> --collection <collection_name> --query="{'key':'value'}" --type=csv --fields 'comma seperated value to be exported' --out <path_to_destination_with.csv_extenstion>

// restore
// command ==> mongoimport
// mongoimport --db <existing or new db_Name> --collection <existing or new collection> --type=csv <path_to_destination_file_with.csv_extension> --headerline
// ****************DATABASE BACKUP & RESTORE******************
