// import nodejs fs module

const fs = require('fs');

// WRITE
// fs.writeFile('./contents/abc1.js', 'welcome to nodejs',function(err,done){
//     if(err){
//         console.log('error is >>',err)
//     }else{
//         console.log('success is >>',done)
//     }
// })

// task 
// make it functional
// and export it
// and call it from another file


function myWrite(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./contents/' + fileName, content, function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve('success in write')
            }
        })
    })

}

// result must be handled
// myWrite('kishor.txt', 'i am nodejs', function (err, done) {
//     if (err) {
//         console.log('err is >>', err);
//     } else {
//         console.log('success is >>', done)
//     }
// })

// myWrite('ac.js', 'sdlfkjsdlkf')
//     .then(function (data) {
//         console.log('success in promise >', data)
//     })
//     .catch(function (err) {
//         console.log('error >>', err);
//     })

module.exports = {
    write: myWrite
};

// READ
// fs.readFile('./contents/ac.js', 'UTF-8', function (err, done) {
//     if (err) {
//         console.log('error reading >>', err);
//     } else {
//         console.log('success in reading >>', done)
//     }
// })

// your task
// createa Read function
// handle result of your function
// file -file communication


// RENAME
// fs.rename('./contents/kishor.txt','./contents/new-kishor.txt',function(err,done){
//     if(err){
//         console.log('error in renaming');
//     }else{
//         console.log('success in renaming ')
//     }
// })

// your task
// create rename function
// handle result of your function
// file -file communication

// REMOVE
// fs.unlink('./contents/ac.js',function(err,done){
//     if(err){
//         console.log('error removing file');
//     }else{
//         console.log('success in removing')
//     }
// })

// your task
// create remove function
// handle result of your function
// file -file communication


