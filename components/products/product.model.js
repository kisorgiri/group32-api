const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    point: {
        type: Number,
        min: 1,
        max: 5
    },
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    color: String,
    brand: String,
    manuDate: Date,
    expiryDate: Date,
    salesDate: Date,
    purchaseDate: Date,
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    description: String,
    price: Number,
    size: String,
    offers: [String],
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'quantity', 'value']
        },
        discountValue: String
    },
    warrentyStatus: Boolean,
    warrentyPeroid: String,
    reviews: [reviewSchema],
    tags: [String],
    images: [String],
    modelNo: String

}, {
    timestamps: true
})

module.exports = mongoose.model('product', ProductSchema);
