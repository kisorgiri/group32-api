const ProductQuery = require('./product.query');

function get(req, res, next) {
    var condition = {};
    if (req.user.role != 1) {
        condition.vendor = req.user._id;
    }
    ProductQuery
        .find(condition, req.query)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

}

function getById(req, res, next) {
    var condition = { _id: req.params.id };
    // todo prepare condition
    ProductQuery
        .find(condition)
        .then(function (data) {
            if (!data[0]) {
                return next({
                    msg: 'Product Not Found',
                    status: 404
                })
            }

            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function insert(req, res, next) {
    // data preparation
    const data = req.body;
    // add meta information in data
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    data.vendor = req.user._id;
    if (req.files) {
        data.images = req.files.map(function (item) {
            return item.filename;
        })
    }
    ProductQuery
        .insert(data)
        .then(function (response) {
            res.json(response);
        })
        .catch(function (err) {
            next(err);
        })

}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (response) {
            if (!response) {
                return next({
                    msg: 'Product Not Found',
                    status: 404
                })
            }
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function update(req, res, next) {
    // data preparation
    const data = req.body;
    data.vendor = req.user._id;

    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    if (req.files) {
        data.images = req.files.map(function (item) {
            return item.filename;
        })
    }
    // TODO remove old files

    ProductQuery
        .update(req.params.id, req.body)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {
    var data = req.body;
    if (req.method === 'GET') {
        data = req.query
    }

    var searchCondition = {};
    ProductQuery.map_product_req(searchCondition, data);

    if (data.minPrice) {
        searchCondition.price = {
            $gte: data.minPrice
        }
    }

    if (data.maxPrice) {
        searchCondition.price = {
            $lte: data.maxPrice
        }
    }

    if (data.minPrice && data.maxPrice) {
        searchCondition.price = {
            $gte: data.minPrice,
            $lte: data.maxPrice
        }
    }

    if (data.fromDate && data.toDate) {
        const fromDate = new Date(data.fromDate).setHours(0, 0, 0, 0); // returns miliseconds
        const toDate = new Date(data.toDate).setHours(23, 59, 59, 999); // returns miliseconds

        searchCondition.createdAt = {
            $lte: new Date(toDate),
            $gte: new Date(fromDate)
        }
    }

    if (data.tags) {
        searchCondition.tags = {
            $in: data.tags.split(',')
        }
    }


    console.log('search condition >>', searchCondition)

    ProductQuery
        .find(searchCondition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function addReview(req, res, next) {
    const data = req.body;
    data.user = req.user._id;
    ProductQuery
        .addReview(req.params.product_id, data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}


module.exports = {
    get: get,
    getById: getById,
    insert: insert,
    update: update,
    remove: remove,
    search: search,
    addReview: addReview
}
