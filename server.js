const http = require('http'); // nodejs inbuilt module
const fileOp = require('./file');

var server = http.createServer(function (request, response) {
    // this callback is executed whenever a clinet request arrived
    // request or 1st argument is always http request object
    // response or 2nd argument is always http response object
    // request response cycle must be completed
    // 
    console.log('client connected to server');
    // console.log('request url is >>',request.url);
    // console.log('request method is >>',request.method);
    // response must be sent 
    // regardless of any method and url this callback is executed for every http client request call
    // response.end('Wecome to Nodejs Server');
    if (request.url === '/write') {
        fileOp.write('filename.txt', 'i am nodejs')
            .then(function (data) {
                response.end('success in write');
            })
            .catch(function (err) {
                response.end('failure in write');
            })
    } else if (request.url === '/read') {

    } else {
        response.end('nothing to perform')
    }
})

server.listen(9000, 'localhost', function (err, done) {
    if (err) {
        console.log('server listening failed')
    }
    else {
        console.log('server listening at port 9000')
        console.log('press CTRL + C to exit');
    }
})
