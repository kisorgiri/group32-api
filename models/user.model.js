const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
    // db_modelling
    name: String,
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    phoneNumber: {
        type: Number
    },
    address: {
        tempAddress: [String],
        permanentAddress: String
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    dob: {
        type: Date
    },
    role: {
        type: Number, //1 for admin, 2 for enduser,
        default: 2
    },
    isActivated: {
        type: Boolean,
        default: true
    },
    image: String,
    passwordResetTokenExpiry: Date,
    passwordResetToken: String
}, {
    timestamps: true // automate created and updatedTime
})

const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;
