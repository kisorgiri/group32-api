// 2009 ===> ryan dahl
// package.json
// package.json file hold overall information about project
// dependencies ==> this sections will hold the entire information about dependant packages

// package-lock.json==> this file is used to lock the exact version of packages installed from npmjs
// package.lock.json file used to store the version of all dependent pacakges

// node_modules ==> node_modules is place to keep all the installed module(packages)

// command
// node -v  check version node
// npm -v check version npm

// npm init  ==> it is used to start a JS project ==> it will helps to create package.json file
// npmjs.com ==> it is global repository that holds overall packages related to JS ecosystem
// npm install <package_name>  => it will install the selected package from npmjs.com
// downloaded pacakges information are kept in package-lock.json file 
// installed pacakges are managed in node_modules folder
// all pacakges of dependency tree are installed inside node_modules folder


// npm uninstall <pacakge_name> remove selected packages from js project

// file-file communication
// import
// const abcd = require('patH_to_own_file');
// const xyz = require('module/pacakges name')
// pacakges/module ==> thses pacakges can be pacakges from nodejs internal module or from node_modules folder



// client-server architecture
// protocol ==> set of rules defined to communicate between two programmes
// http 
// ftp
// smtp
// mqtt

// in web protocol used is HTTP
// http protocol
// status code
// 200 ==> success range
// 300 ==> validation range
// 400===> application error
// 500 ==> server error
// http verb(method)
// GET,PUT,POST,DELETE,PATCH
// request and response

// in web
// webserver  ==> http server
// web client ==> http client


// REST API

// API(APplication Programming Interface)
// ===> endpoint (combination of http method and url)
// eg post /login get /user put/user delete/notification

// REST (Representational State Transfer)
// following points needs to be fullfilled to be on REST 
// 1. Stateless 
// 2. correct use of http verb
// GET===> data fetch request
// POST===> data insert request
// PUT/PATCH==> data update request
// DELETE==> data remove request
// 3. data format must be either in JSON or XML
// 4. caching GET  (investigate)

// CI/CD (devops)
// CRON JOB

// db script

// relational db
// aws services


// road map for FrontEnd
// html/css
// es6
// react introduction



// we have created a REST SERVER using node/express
