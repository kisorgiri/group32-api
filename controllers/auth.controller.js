const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const mapUserReq = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const passwordHash = require('password-hash');
const nodemailer = require('nodemailer');
const randomStr = require('random-string');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareMail(data) {
    return {
        from: 'Group 32 Store', // sender address
        to: "basant.brd@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Forgot Password?", // plain text body
        html: `<p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system please use link below to reset your password</p>
        <p><a href="${data.link}">click here to reset password</a></p>
        <p>If you have not requested to reset password please kindly ignore this email.</p>
        <p>Regards</p>
        <p>Group 32 Support Team</p>`, // html body
    }
}

function createToken(user) {
    let token = jwt.sign({
        _id: user._id,
    }, config.JWT_SECRET)
    return token;
}

// router.get('/', function (req, res, next) {
//     require('fs').readFile('.lsdlkjf.js', function (err, done) {
//         if (err) {
//             return req.myEvent.emit('error', err, res);
//         }
//     })
// })

router.post('/login', function (req, res, next) {
    //    db connection
    UserModel
        .findOne({
            $or: [
                {
                    username: req.body.username

                },
                {
                    email: req.body.username

                }
            ]
        })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'Invalid Username',
                    status: 400
                })
            }
            if (!user.isActivated) {
                return next({
                    msg: "Your account is disabled please contact system administrator",
                    status: 401
                })
            }
            if (user) {
                // password verification TODO
                // res.json(user);
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {

                    // token generation
                    var token = createToken(user);
                    res.json({
                        user: user,
                        token: token
                    })
                } else {
                    return next({
                        msg: "Invalid Password",
                        status: 400
                    })
                }

            }
        })
})

router.post('/register', uploader.single('img'), function (req, res, next) {
    console.log('req.body >>', req.body);
    console.log('req.file >>', req.file);
    // db stuff
    // if (req.fileTypeError) {
    //     return next({
    //         msg: "Invalid file format",
    //         status: 400
    //     })
    // }
    const newUser = new UserModel({}); // newUser is mongoose Object

    if (req.file) {
        const mimeType = req.file.mimetype.split('/')[0]
        console.log('mime Type >>', mimeType)
        if (mimeType !== 'image') {
            // fs module inject garera remove uploaded file
            return next({
                msg: "Invalid File Format",
                status: 400
            })
        }
        req.body.image = req.file.filename;
    }
    const newMappedUser = mapUserReq(newUser, req.body);
    // newnewUser is mongoose Object;
    // methods of mongoose
    // newUser.save(function (err, done) {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.json(done);
    // })
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser
        .save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

})


router.post('/forgot-password', function (req, res, next) {

    UserModel.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Email not registered yet",
                status: 404
            })
        }

        const passwordResetToken = randomStr({
            length: 23
        });
        const passwordResetTokenExpiry = Date.now() + (1000 * 60 * 60 * 24 * 2);
        // proceed with email sending
        var emailData = {
            name: user.username,
            email: user.email,
            link: `${req.headers.origin}/reset_password/${passwordResetToken}`
        }


        var emailContent = prepareMail(emailData);

        user.passwordResetToken = passwordResetToken;
        user.passwordResetTokenExpiry = passwordResetTokenExpiry;

        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            sender.sendMail(emailContent, function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
    })

})

router.post('/reset-password/:token', function (req, res, next) {
    const token = req.params.token;

    UserModel.findOne({
        passwordResetToken: token,
        passwordResetTokenExpiry: {
            $gte: Date.now()
        }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Invalid or Expired Password reset token",
                status: 404
            })
        }
        // if withing time frame
        user.password = passwordHash.generate(req.body.password);
        user.passwordResetToken = null;
        user.passwordResetTokenExpiry = null;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            res.json(saved);
        })
    })
})

module.exports = router;
