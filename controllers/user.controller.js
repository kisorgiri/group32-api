const router = require('express').Router();
const UserModel = require('./../models/user.model');
const mapUserReq = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader');
router.route('/')
    .get(function (req, res, next) {
        console.log('loggedInUser >>', req.user);
        //   get all user
        var condition = {};
        // projection
        UserModel
            .find(condition)
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip()
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            });

    })
    .post(function (req, res, next) {

    });

router.route('/search')
    .get(function (req, res, next) {
        res.send('from search of user')
    })
    .post(function (req, res, next) {

    });

router.route('/:id')
    .get(function (req, res, next) {
        UserModel
            .findById(req.params.id)
            .then(function (user) {
                if (!user) {
                    return next({
                        msg: "User not found",
                        status: 404
                    })
                }
                res.json(user)
            })
            .catch(function (err) {
                next(err);
            })
    })
    .put(uploader.single('img'), function (req, res, next) {
        if (req.fileTypeError) {
            return next({
                msg: "Invalid file format",
                status: 400
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User Not Found',
                    status: 404
                })
            }
            // user found now update
            // user is mongoose object
            if (req.file) {
                req.body.image = req.file.filename
            }

            var updatedMappedUser = mapUserReq(user, req.body);

            updatedMappedUser.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                // once updated if there is new file in request remove old file
                // server clean up
                res.json(updated);
            })

        })
    })
    .delete(function (req, res, next) {
        if (req.user.role !== 1) {
            return next({
                msg: 'You Dont Have Access',
                status: 403
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed);
            })
        })
    });


module.exports = router;

