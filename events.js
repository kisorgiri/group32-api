const events = require('events');

// create instance
const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();

setTimeout(function () {
    myEvent1.emit('kishor', 'welcome to events'); // event tirgger
}, 3000)

myEvent.on('kishor', function (data) { // event listener
    console.log('thic block will be executed once kishor event is fired', data);
})

