const { urlencoded } = require('express');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
const app = express(); // app is now entire express framework
// event stuff
// const events = require('events');
// const myEvent = new events.EventEmitter();

// app.use(function (req, res, next) {
//     req.myEvent = myEvent
//     next();
// })

// myEvent.on('error', function (err, res) {
//     console.log('at own error handler >>', err)
//     res.json(err)
// })


require('./socket.js')(app)

require('./db');

app.set('port', 8000);
// view engine setup
const pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(__dirname, 'views'));
// view engine setup

const apiRoute = require('./api.routes');


// load third party middleware
app.use(morgan('dev'));
app.use(cors()) // accept all origin

// load inbuilt middleware
app.use(express.static('uploads')) // internal serve
app.use('/file', express.static(path.join(__dirname, 'uploads'))); //external request serve
// incoming data must be parsed
// parser for x-www-form-urlencoded
app.use(urlencoded({
    extended: true
}))
// json parser
app.use(express.json());
// parser middleware will parse incoming data and add those data in req.body property

// load routing level middleware
// mount incoming requst
app.use('/api', apiRoute)

// 404 handler
app.use(function (req, res, next) {
    // application level middleware
    next({
        msg: 'Not Found',
        status: 404
    })
})

// error handling middleware must be called to get executed
// to call error handling middleware we will call next with argument
// the arugment sent in next is first value of error handling middleware
app.use(function (err, req, res, next) {
    // err or 1st arugment is argument passed inside next
    // req or 2nd http request
    // res or 3rd response 
    // next or 4th next reference
    // TODO set status code in the error response
    res.status(err.status || 400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('err is >>', err);
    } else {
        console.log('server listening at port ' + app.get('port'));
        console.log('press CTRL + C to exit');
    }
})

// middleware 
// middlware is a function which has access to http request object http response object and next middleware function reference
// middlware always came into action in between request response cycle
// middlware is very powerful function which can modify request and complete response

// THE ORDER OF MIDDLEWARE IS VERY IMPORTANT

// SYNTAX
// function(req,res,next){
//     // req or 1st argument is for http request object
//     // res or 2nd argument is for http response object
//     // next or 3rd argument is for next middleware reference
// }
// configuration of middleware
// app.use() // configuration block of middleware


// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middleware

// application level middleware
// middleware having scope of req, res and next are application level middleware
